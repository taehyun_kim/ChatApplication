package chatapp.server;

import chatapp.http.HttpStaticFileHandler;
import chatapp.server.handler.WebChatHandler;
import chatapp.server.handler.WebSocketHandshakeHandler;
import chatapp.util.NettyStartupUtil;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;

public class WebChatServer {
	private static final String index=System.getProperty("user.dir")+"/index.html";
	
	public static void main(String[] args) throws Exception {
		NettyStartupUtil.runServer(8040, pipeline->{
			pipeline.addLast(new HttpServerCodec()); // TODO HTTPServerCode�� �ϴ°� ����?
			pipeline.addLast(new HttpObjectAggregator(66536));
            pipeline.addLast(new WebSocketHandshakeHandler("/chat", new WebChatHandler()));
            pipeline.addLast(new HttpStaticFileHandler("/", index));
		});
	}
}
